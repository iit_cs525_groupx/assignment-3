#Record Manager

---

###Members

---

- Abdulwahab Alothaim
- Yitao Ma
- Li Huang
- Sawsane Ouchtal

---

###File list

---

- dberror.c
- dberror.h
- buffer_mgr.c
- buffer_mgr.h
- buffer_mgr_stat.c
- buffer_mgr_stat.h
- dt.h
- expr.h
- record_mgr.c
- record_mgr.h
- storage_mgr.c
- storage_mgr.h
- test_assign3.c
- test_expr.c
- test_helper.h
- README.txt
- Makefile

---

###Installation instruction

---

using test_expr.c and test_assign3.c to test

after test, use clean to delete files except source code

---

###Function descriptions

---

###Table and Record Manager Functions

#####By: Li Huang

---

There are functions to initialize and shutdown a record manager. Furthermore, there are functions to create, open, and close a table. Creating a table should create the underlying page file and store information about the schema, free-space, ... and so on in the Table Information pages. All operations on a table such as scanning or inserting records require the table to be opened first. Afterwards, clients can use the RM_TableData struct to interact with the table. Closing a table should cause all outstanding changes to the table to be written to the page file. The getNumTuples function returns the number of tuples in the table.

---

###Record Function

#####By: Abo Othaim Alothaim

---

These functions are used to retrieve a record with a certain RID, to delete a record with a certain RID, to insert a new record, and to update an existing record with new values. When a new record is inserted the record manager should assign an RID to this record and update the record parameter passed to insertRecord.

---

###Scan Function

#####By: Li Huang & Yitao MA

---

A client can initiate a scan to retrieve all tuples from a table that fulfill a certain condition (represented as an Expr). Starting a scan initializes the RM_ScanHandle data structure passed as an argument to startScan. Afterwards, calls to the next method should return the next tuple that fulfills the scan condition. If NULL is passed as a scan condition, then all tuples of the table should be returned. next should return RC_RM_NO_MORE_TUPLES once the scan is completed and RC_OK otherwise (unless an error occurs of course). Below is an example of how a client can use a scan.

###Schema Function

#####By: Yitao MA

---

These helper functions are used to return the size in bytes of records for a given schema and create a new schema.

###Attribute Functions

#####By: Sawsane Ouchtal

---

These functions are used to get or set the attribute values of a record and create a new record for a given schema. Creating a new record should allocate enough memory to the data field to hold the binary representations for all attributes of this record as determined by the schema.



