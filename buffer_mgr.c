#include<stdio.h>
#include<stdlib.h>
#include "buffer_mgr.h"
#include "storage_mgr.h"
#include <math.h>



#define FIX_PAGE 0;

// PageFrame is a New data type to handle new variables needed to be stored with each page in buffer Manager
// better soulution is to edit the BM_PageHandle BUT the test designed not to accept changes @OTh
typedef struct PageFrame {
	SM_PageHandle data;
	PageNumber pageNum; // An identification integer given to each page
	int dirty;// use this to indicate the page be modified or not 0=false , 1=true @OTh
	int fixCount;// fixCount is based on pinning/un-pinning request. Tell how many clients using this page
	int num_of_fixs; // For LRU Replacment Stratgy. Count the number page were
										//used (bring from disk + used in momery) @OTh
} PageFrame;

int NumReadIO= 0; //count the number of reads
int NumWriteIO = 0 ;// count the number of writes @OTh

// Used by LUR this count all writes on a buffer manager
// (include the increment of Fixcount for exiting page @OTh)
int num_of_writes = 0;

// used by FIFO this count all writes on a buffer manager (Only include if page reought from disk) @OTh
int num_of_writes_expt_incs_fix = 0;
int bm_size = 0; //the numpages for buffer manager

//For testing porpuses @OTh
void printallpagesinbf (PageFrame *allPages){
	printf("|| pagenumber -- fixCount --  dirty -- num_of_fixs || \n");
	int i;
	for(i = 0; i < bm_size; i++){
		printf("|| %d   --	 %d   -- 	 %d   -- 	%d  	  || \n",allPages[i].pageNum,allPages[i].fixCount,
	allPages[i].dirty,allPages[i].num_of_fixs );
	}
}


//  ** Replacment Strategy functions //
// This is a FIFO replacment strategy for our buffer manager
// /FIFO (First Come First ) @OTh
void FIFO(BM_BufferPool *const bm,  PageFrame *const page)
{
	//printf("I entered FIFO FUNCTION \n");

	PageFrame *getpagesfrombm = (PageFrame  *) bm->mgmtData;


	int i, bm_front_pg;
	bm_front_pg = num_of_writes_expt_incs_fix % bm_size;
	//printf("											bm_front_pg: %d = num of writes expt incs fix %d (o|o) bm_size %d  \n", bm_front_pg, num_of_writes_expt_incs_fix, bm_size);

	// Go through Buffer Manager bm to find a victom starting from the oldest added page @OTh
	for(i = 0; i < bm_size; i++){
		if(getpagesfrombm[bm_front_pg].fixCount == 0)
		{ //this is a victim @OTh

			if(getpagesfrombm[bm_front_pg].dirty == 1)
			{ // Data in victim changed, we need to write the change to disk @OTh
				SM_FileHandle fh;
				openPageFile(bm->pageFile, &fh);
				writeBlock(getpagesfrombm[bm_front_pg].pageNum, &fh, getpagesfrombm[bm_front_pg].data);
				NumWriteIO++;  // add 1 to the number of writes on disk @OTh
			}
			// replacing the victim whith the new page @OTh
			getpagesfrombm[bm_front_pg].pageNum = page->pageNum;
			getpagesfrombm[bm_front_pg].data = page->data;
			getpagesfrombm[bm_front_pg].dirty = page->dirty;
			getpagesfrombm[bm_front_pg].fixCount = page->fixCount;
			getpagesfrombm[bm_front_pg].num_of_fixs = page->num_of_fixs;
			break;
		}
		else
		{			// That's mean, someone using the current page. So, move to the next to find a victim
			bm_front_pg++;
			if (bm_front_pg % bm_size == 0){
				bm_front_pg = 0;
			}else bm_front_pg = bm_front_pg; // to go back to the begining of array
		}
	}
}

//   function
// This is a LRU replacment strategy for our buffer manager
// LRU  (Least Recently Used) @OTh
void LRU (BM_BufferPool *const bm, PageFrame *const page)
{  // in this strategy we will have two for loops one is to find the first victim, then go through another loop
	// to compare this victim to other and change the victim if we find a nicer victim :) nicer=
	// few clients use this page = lower num of fixs

	//printf("I entered LRU FUNCTION \n");
	PageFrame *getpagesfrombm = (PageFrame  *) bm->mgmtData;
	int i, least_FixsNum_page_index, least_num_of_fixs ;
	least_FixsNum_page_index = -1;

	// find the first page which no client is using. Assume it's the victim
	for(i = 0; i < bm_size; i++)
	{
		if(getpagesfrombm[i].fixCount == 0){
			least_FixsNum_page_index = i; //this is the victim right now
			least_num_of_fixs = getpagesfrombm[i].num_of_fixs; // store to compare in next step
			break;
		}
	}

	// Go through all pages to find the a best page to be victim (the one that have min num_of_fixs) @OTh
	for(i = least_FixsNum_page_index + 1; i < bm_size; i++){
		if(getpagesfrombm[i].num_of_fixs < least_num_of_fixs){
			least_FixsNum_page_index = i;
			least_num_of_fixs = getpagesfrombm[i].num_of_fixs;
		}
	}

	if(getpagesfrombm[least_FixsNum_page_index].dirty == 1)
	{ // Data in victim changed, we need to write the change to disk @OTh
		SM_FileHandle fh;
		openPageFile(bm->pageFile, &fh);
		writeBlock(getpagesfrombm[least_FixsNum_page_index].pageNum, &fh,
			getpagesfrombm[least_FixsNum_page_index].data);
		NumWriteIO++;  // add 1 to the number of writes on disk @OTh
	}
	// replacing the victim whith the new page @OTh
	getpagesfrombm[least_FixsNum_page_index].pageNum = page->pageNum;
	getpagesfrombm[least_FixsNum_page_index].data = page->data;
	getpagesfrombm[least_FixsNum_page_index].dirty = page->dirty;
	getpagesfrombm[least_FixsNum_page_index].fixCount = page->fixCount;
	getpagesfrombm[least_FixsNum_page_index].num_of_fixs = page->num_of_fixs;

}

// ******************************END OF *************************//
//   ** Replacment Strategy functions //



//  ** Buffer Manager Interface Pool Handling **  //

/*creates a new buffer pool with numPages page frames using the page replacement strategy strategy.
 Initially, all page frames should be empty.The page file should already exist, this method should
 not generate a new page file.stratData can be used to pass parameters for the page replacement
 strategy. For example, for LRU-k this could be the parameter k.*/

RC initBufferPool(BM_BufferPool *const bm, const char *const pageFileName,
                  const int numPages, ReplacementStrategy strategy, void *stratData)
{
	//printf("I entered initBufferPool FUNCTION. BM: %s has numPages for BM: %d \n",pageFileName ,numPages);
	int i;
	bm_size = numPages; //local buffer size @OTh

	bm->pageFile = (char*)pageFileName;
  bm->numPages = numPages;
  bm->strategy = strategy;
  PageFrame *page = malloc(sizeof(PageFrame) * numPages);

	// create empty page keeper to store in mgmData
  for(i = 0; i < bm_size; i++)
  {
		page[i].pageNum = -1;
    page[i].data = NULL;
    page[i].dirty = 0;
    page[i].fixCount = 0;
		page[i].num_of_fixs= 0;
	}
	//printallpagesinbf(page);


	bm->mgmtData = page;
	NumWriteIO = 0;
	num_of_writes = 0; //for LUR @OTh
	num_of_writes_expt_incs_fix = 0; // For FIFO @OTh
	return RC_OK;
}

/*shutdownBufferPool destroys a buffer pool.This method should free
 up all resources associated with buffer pool.it should free the
 memory allocated for page frames.If the buffer pool contains any
 dirty pages, then these pages should be written back to disk before
 destroying the pool.It is an error to shutdown a buffer pool that has
 pinned pages.*/

RC shutdownBufferPool(BM_BufferPool *const bm){
	//printf("I entered shutdownBufferPool FUNCTION \n");
	PageFrame *page = (PageFrame *)bm->mgmtData;
	forceFlushPool(bm);
	int i;
	for(i = 0; i < bm_size; i++)
	{
		// If fixCount != 0, it means that the contents of the page was modified by some client and has not been written back to disk.
		if(page[i].fixCount != 0) {
			printf("Shutdown failed\n");
		}
	}
	// free the space that was used by page @OTh
	free(page);
	bm->mgmtData = NULL;
	return RC_OK;
}

/*forceFlushPool causes all dirty pages (with fix count 0) from the
 buffer pool to be written to disk.*/

RC forceFlushPool(BM_BufferPool *const bm){
	//printf("I entered forceFlushPool FUNCTION \n");
	PageFrame *page = (PageFrame *)bm->mgmtData;
	int i ;
	// Go through the pool force closing pages
	for(i = 0; i < bm_size; i++)
	{
		if(page[i].fixCount == 0 && page[i].dirty == 1)
		{ // write on the disk
			SM_FileHandle fh;
			openPageFile(bm->pageFile, &fh);
			writeBlock(page[i].pageNum, &fh, page[i].data);
			page[i].dirty = 0; // set dirt now to 0 since we wrote back to disk @OTh
			NumWriteIO++; // increase the write counter @OTh
		}
	}
	return RC_OK;
}

// ******************************END OF *************************//
//  ** Buffer Manager Interface Pool Handling **  //



//  ** Buffer Manager Interface Access Pages  //
// @ALL :) Finally I the issues here @OTh #nightmare
RC markDirty (BM_BufferPool *const bm, BM_PageHandle *const page){
	//printf("I entered markDirty FUNCTION \n");
	PageFrame *pagefr = (PageFrame *)bm->mgmtData;
	int i;
	// go the pool to find page, if found change its Dirty to 1 @OTh
	for(i = 0; i < bm_size; i++)
	{
		if(pagefr[i].pageNum == page->pageNum)
		{
			pagefr[i].dirty = 1;
			return RC_OK;
		}
	}
	return RC_OK;

}

RC unpinPage (BM_BufferPool *const bm, BM_PageHandle *const page){
	//printf("I entered unpinPage FUNCTION to unPin Below page \n");

	PageFrame *pagefr = (PageFrame *)bm->mgmtData;
	int i;
	// Go through the pool to find page and unpin from the pool by decrease the num of its fixCount @OTh
	for(i = 0; i < bm_size; i++)
	{
		if(pagefr[i].pageNum == page->pageNum)
		{
			pagefr[i].fixCount--;
			//printf("unPined page number: %d fixcount is: %d \n", pagefr[i].pageNum, pagefr[i].fixCount );
			break;
		}
	}
	return RC_OK;
}

RC forcePage (BM_BufferPool *const bm, BM_PageHandle *const page){
	//printf("I entered forcePage FUNCTION \n");

	PageFrame *pagefr = (PageFrame *)bm->mgmtData;
	int i;
// Write page to disk before close @OTh
	for(i = 0; i < bm_size; i++)
	{
		if(pagefr[i].pageNum == page->pageNum){
			SM_FileHandle fh;
			openPageFile(bm->pageFile, &fh);
			writeBlock(pagefr[i].pageNum, &fh, pagefr[i].data);
			pagefr[i].dirty = 0;

			num_of_writes++; //used by LUR @OTh
			num_of_writes_expt_incs_fix++; //Used by FIFO @OTh
		}
	}
    return RC_OK;
}

// >> this is the nightmare ;)
RC pinPage (BM_BufferPool *const bm, BM_PageHandle *const page,
	    const PageNumber pageNum){
	//printf("I entered pinPage FUNCTION \n");
	PageFrame *allPages = (PageFrame  *) bm->mgmtData;

	if(allPages[0].pageNum == -1)
	{ // Means bm is empty @OTh
		SM_FileHandle fh;
		openPageFile(bm->pageFile, &fh);
		allPages[0].data = (SM_PageHandle) malloc(PAGE_SIZE);
		ensureCapacity(pageNum,&fh);
		readBlock(pageNum, &fh, allPages[0].data);
		num_of_writes = 0;
		num_of_writes_expt_incs_fix=0;
		allPages[0].pageNum = pageNum;
		allPages[0].fixCount++;
		allPages[0].dirty = 0;
		allPages[0].num_of_fixs = num_of_writes;
		page->pageNum = pageNum;
		page->data = allPages[0].data;
		//printf("page number: %d fixcount is: %d \n", allPages[0].pageNum, allPages[0].fixCount );

		//printf("									FROM BM EMPTY - THE Num of Write is: %d  \n", num_of_writes);
		return RC_OK;
	}
	else
	{	// The bm pool is not empty . So, we need to check
		//  (if page is there increse fixCount, if not add to pool)
		int i;
		// We will go through the pool to (increase exist or add new). If pool finished without fining exsist
		//  nor adding the page, it's mean the pool is full. @OTh

		for(i = 0; i < bm_size; i++)
		{
			if(allPages[i].pageNum != -1)
			{
				if(allPages[i].pageNum == pageNum)
				{ // page is in bm @OTh
					allPages[i].fixCount++;
					allPages[i].num_of_fixs = num_of_writes;
					num_of_writes++;
					//printf("page number: %d fixcount is: %d \n", allPages[i].pageNum, allPages[i].fixCount );
					//printallpagesinbf(allPages);

					page->pageNum = pageNum;
					page->data = allPages[i].data;
					//printf("									FROM found THE PAGE IN bm ");

					return RC_OK;
				}
			} else { //reach to empty spot and could not find the page in bm @OTh
				// add a new page to bm
				SM_FileHandle fh;
				openPageFile(bm->pageFile, &fh);
				allPages[i].data = (SM_PageHandle) malloc(PAGE_SIZE);
				readBlock(pageNum, &fh, allPages[i].data);
				allPages[i].pageNum = pageNum;
				allPages[i].fixCount = 1;
				allPages[i].num_of_fixs = num_of_writes;
				num_of_writes++;
				num_of_writes_expt_incs_fix++;
				//printf("page number: %d fixcount is: %d \n", allPages[i].pageNum, allPages[i].fixCount );
				//printallpagesinbf(allPages);
				page->pageNum = pageNum;
				page->data = allPages[i].data;

				//printf("									FROM REACH EMPTY SPOT - ");
				return RC_OK;
			}
		}

		// if algorithm reach here (Out of FOr lOOp), that's mean pool is full and we have to use
		//  stratgy to get victim and put new page @OTh

			// Create a new page to store data read from the file.
			PageFrame *newPage = (PageFrame *) malloc(sizeof(PageFrame));

			// Read from disk and store content to page. Then pass this page to Replasment
			// stratgy assosiated with buffer loop @OTh
			SM_FileHandle fh;
			openPageFile(bm->pageFile, &fh);
			newPage->data = (SM_PageHandle) malloc(PAGE_SIZE);
			readBlock(pageNum, &fh, newPage->data);
			newPage->pageNum = pageNum;
			newPage->dirty = 0;
			newPage->fixCount = 1;
			newPage->num_of_fixs = (int) num_of_writes;
			num_of_writes++;
			num_of_writes_expt_incs_fix++;


			page->pageNum = pageNum;
			page->data = newPage->data;
			//printf("BF is FULL, Data before use Replacment_Stratgy: \n");
			//printallpagesinbf(allPages);

			// Call a specific Replacment stratdy function
			//printf("INSERT THE PAGE|| %d   -- 	%d   --  	%d   -- 	%d 	   || \n",newPage->pageNum,newPage->fixCount,
		//newPage->dirty,newPage->num_of_fixs );

		if(bm->strategy == RS_FIFO){
			FIFO(bm, newPage);
			//printf("FIFO is finished, Data AFTER use FIFO: \n");
			//printallpagesinbf(allPages);
		}
		else
		{
			if(bm->strategy == RS_LRU){
				LRU(bm, newPage);
				//printf("LUR is finished, Data AFTER use FIFO: \n");
				//printallpagesinbf(allPages);
			}
			else
			{
				//printf("\n \n OTHER algorithm not implemented \n \n \n");

			}
		}
		//	printf("									FROM USING FIFO or LUR - ");
		//printf("THE Num of Write is: %d  \n", num_of_writes);
		return RC_OK;
	}

}

// ******************************END OF *************************//
//  ** Buffer Manager Interface Access Pages **  //



//  ** Statistics Interface  //

PageNumber *getFrameContents (BM_BufferPool *const bm)
{
	//printf("I entered getFrameContents FUNCTION \n");

    PageFrame *getframecontents = (PageFrame  *) bm->mgmtData;

    // create an array
    int *FC_array = malloc(bm_size * sizeof(int));
		int i;
		// go through the pool to fill the array @OTh
    for (i = 0; i < bm_size; i++)
    {
        if(getframecontents[i].pageNum == NO_PAGE)
        {
            FC_array[i] = NO_PAGE;
        }
        else
            FC_array[i] = getframecontents[i].pageNum;

    }
    return FC_array;
}


bool *getDirtyFlags (BM_BufferPool *const bm)
{
	//printf("I entered getDirtyFlags FUNCTION \n");

    PageFrame *getdirtyflags = (PageFrame  *) bm->mgmtData;

    // create an array
    bool *DF_array = malloc(bm_size * sizeof(bool));
		int i;
		// go through the pool to fill the array @OTh
    for (i = 0; i < bm_size; i++)
    {
        if(getdirtyflags[i].dirty == 1)
        {
            DF_array[i] = true;
            //printf("Page error!");
        } else
            DF_array[i] = false;
    }
    return DF_array;
}


int *getFixCounts (BM_BufferPool *const bm)
{
	//printf("I entered getFixCounts FUNCTION \n");
	int i;

    PageFrame *getfixcounts = (PageFrame  *) bm->mgmtData;
    // create an array
    int *FCO_array = malloc(bm_size * sizeof(int));
		// go through the pool to fill the array @OTh
    for (i = 0; i < bm_size; i++)
    {
        if(getfixcounts[i].fixCount == NO_PAGE)
        {
            FCO_array[i] = FIX_PAGE;
            //printf("Page error!");
        }
        else
            FCO_array[i] = getfixcounts[i].fixCount;
    }
    return FCO_array;
};

int getNumReadIO (BM_BufferPool *const bm)
{
	//printf("I entered getNumReadIO FUNCTION \n");
	NumReadIO = num_of_writes_expt_incs_fix + 1;
    return NumReadIO;
}


int getNumWriteIO (BM_BufferPool *const bm)
{
	//printf("I entered getNumWriteIO FUNCTION \n");

    return NumWriteIO;

}
// ******************************END OF *************************//
//  ** Statistics Interface ** //
